const mng = require('mongoose');

const masterSchema = mng.Schema({
	accountFrom:{
		type: String,
		default: '_Untitled'
	},
  accountTo:{
    type: String,
		default: '_Untitled'
  },
	amount:{
		type: Date,
		default: Date.now
	},
  description:{
    type: String,
		default: '_Untitled'
  },
  transactionDate:{
		type: Date,
		default: Date.now
	},
});

const M = module.exports = mng.model('Note', masterSchema);

module.exports.getTransactions = (callback, limit) => {
	M.find(callback).limit(limit);
}
