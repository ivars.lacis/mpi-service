const mng = require('mongoose');

const noteSch = mng.Schema({
	title:{
		type: String,
		default: ' _Untitled'
	},
  note_body:{
    type: String
  },
	create_date:{
		type: Date,
		default: Date.now
	},
  modified_date:{
		type: Date,
		default: Date.now
	}
});

const Note = module.exports = mng.model('Note', noteSch);
// couch vai coutch
module.exports.getNotes = (callback, limit) => {
	Note.find(callback).limit(limit);
}

module.exports.getNoteById = (id, callback) => {
	Note.findById(id, callback);
}

module.exports.addNote = (note, callback) => {
	Note.create(note, callback);
}

module.exports.updateNote = (id, note, options, callback) => {
	var u = {
		title: note.title,
    note_body: note.note_body,
    modified_date: Date.now()
	}
	Note.findOneAndUpdate({_id: id}, u, options, callback);
}

module.exports.removeNote = (id, callback) => {
	var q = {_id: id};
	Note.remove(q, callback);
}
